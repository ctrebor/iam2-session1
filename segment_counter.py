#!/usr/bin/python3

def count_segments(x):
    index = 0
    count = 1
    while index <= len(x) - 1:
        if index == len(x) - 1:
            count += 0
        elif x[index] == x[index + 1]:
            count += 0
        else:
            count += 1
        index += 1
    return count


segment = ''
while segment != 'quit':
    segment = input('Enter a string to count segments for or enter \'quit\' to quit: ')
    if segment != 'quit':
        print(count_segments(segment))

