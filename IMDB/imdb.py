#!/usr/bin/python3

from prettytable import PrettyTable
import psycopg2

# get movie from user
movie = input('What movie(s) would you like to lookup? ')

# connect to postgresql database
conn = psycopg2.connect(host='127.0.0.1', dbname='rcramer')

# create cursor
cur = conn.cursor()

# execute select query
cur.execute("SELECT * FROM imdb_basic WHERE titletype = 'movie' AND primarytitle LIKE %s and isadult is False ORDER BY startyear;", (movie,))

# fetch result set
records = cur.fetchall()

# commit
conn.commit()

# create pretty table instance pt and configure 
pt = PrettyTable()
pt.field_names = ["tconst", "titletype", "primarytitle", "originaltitle", "isadult", "startyear", "endyear", "runtimemin", "genres"]
pt.align["tconst"] = "l"
pt.align["titletype"] = "l"
pt.align["primarytitle"] = "l"
pt.align["originaltitle"] = "l"
pt.align["isadult"] = "l"
pt.align["startyear"] = "l"
pt.align["endyear"] = "l"
pt.align["runtimemin"] = "l"
pt.align["genres"] = "l"

# add result set to pretty table
for record in records:
    pt.add_row(record)

# print pretty table with database results
print(pt)

# close database connection
cur.close()
conn.close()
