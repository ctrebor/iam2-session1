#!/usr/bin/python3

# define roman numeral dictionary
roman_to_arabic = {
    'M': 1000,
    'D':  500,
    'C':  100,
    'L':   50,
    'X':   10,
    'V':    5,
    'I':    1
}

# get input from user
roman_numerals = input('Enter roman numerals: ')

# check that input is valid
valid = True
for each in roman_numerals:
    if each not in 'MDCLXVI':
        print('Input is not valid.')
        valid = False
        break

# if input is valid, calculate total
if valid == True:
    print('Input is valid.')
    
    # convert roman numeral string into a list of numeric values
    value_list = []
    for numeral in roman_numerals:
        value_list.append(roman_to_arabic[numeral])
        
    # get total by adding or subtracting values in value_list[]
    total = 0
    for x in range(0, len(value_list)):
        if x == len(value_list) - 1:
            total += int(value_list[x])
        elif value_list[x] >= value_list[x+1]:
            total += int(value_list[x])
        elif value_list[x] < value_list[x+1]:
            total -= int(value_list[x])
    print('Total is:', total) 
