#!/usr/bin/python3

word_count_dict = {}
with open('/home/rcramer/Code/iam2-session1/linux-files/hamlet.txt', 'r') as word_file:
    for line in word_file:
        clean_line = ''
        for char in line:    
            if char.isalpha() or char == ' ':
                clean_line += char
        word_list = clean_line.split(" ")
        for word in word_list:
            word_lc = word.lower()
            if word_lc in word_count_dict:
                word_count_dict[word_lc] += 1
            else:
                word_count_dict[word_lc] = 1
    sorted_by_value = sorted(((value, key) for (key, value) in word_count_dict.items()),reverse=True)               
    for pair in sorted_by_value:
        value, key = pair
        print(key, '=', value)
