#!/usr/bin/python3
import random

# Conway's Game of Life

alive = '*'
dead = '.'

def Board_Init(size):
    grid = [[' '] * size for n in range(size)]
    for n in range(0, size):
        for m in range(0, size):
            value = random.randint(1, 100)
            grid[n][m] = alive if value % 4 == 0 else dead
    return grid

def Board_Iterate(size, board):
    # for each cell on board
    for n in range(0, size):
        for m in range(0, size):
            # reset neighbor counters to zero
            alive_neighbors = 0
            dead_neighbors = 0
            cell_status = board[n][m]
            # loop through cells neighbors
            for x_neighbor in range(n - 1, n + 2):
                # make sure x neighbor in range of board
                if 0 <= x_neighbor < size:
                    for y_neighbor in range(m - 1, m + 2):
                        # make sure y neighbor in range of board
                        if 0 <= y_neighbor < size:
                            # check status of neighbors                            
                            if (n == x_neighbor) and (m == y_neighbor):
                                continue
                            elif board[x_neighbor][y_neighbor] == alive:
                                alive_neighbors += 1
                            else:
                                dead_neighbors += 1  
            # change status of cell based on neighbors  
            if cell_status == alive:
                board[n][m] = alive if alive_neighbors in (2, 3) else dead
            else:
                board[n][m] = alive if alive_neighbors == 3 else dead
    return

# get board size from user
board_size = int(input('Enter size of board: '))

# get number of iterations
iters = int(input('Enter number of iterations: '))

# initialize board and print board
board = Board_Init(board_size)
print('== Initial Board ==')
for row in board:
    print(row)

# iterate over board and print board
for it in range(1, iters + 1):
    Board_Iterate(board_size, board)
    print('== Iteration', it ,'==')
    for row in board:
        print(row)

