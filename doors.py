#!/usr/bin/python3

door_status = {}
for trip in range(1, 101):
    for door in range(1, 101):
       if trip == 1:
            door_status[door] = 1
       else:
            if door % trip == 0:
                door_status[door] *= -1
    
    print('--- Trip', trip, '---')
    for pair in door_status.items():
        door, status = pair
        if status == 1:
            status_str = 'OPEN'
        else:
            status_str = 'CLOSED'
        print("Door", door, "is", status_str)
